//= require index

describe('Chart', function() {
  var average, 
      data = [
        {"load_average": 1},
        {"load_average": 2},
        {"load_average": 3},
        {"load_average": 4},
        {"load_average": 5}
      ]

  beforeEach(function() {
    Chart.latestDatapoint = {
      time: new Date(),
      load_average: 2
    }

    spyOn(Chart, 'updateAverage').and.callThrough();
    average = Chart.updateAverage(data);
  });

  it("calls updateAverage", function() {
    expect(Chart.updateAverage).toHaveBeenCalledWith(data);
  })

  it("calculates the correct average", function() {
    expect(average).toBe(3);
  });
});
