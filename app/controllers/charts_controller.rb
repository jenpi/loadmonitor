require 'sysinfo'

class ChartsController < ApplicationController
  def index
    @sysinfo = SysInfo.new
  end

  def get_load_average
    uptime = {
      time: Time.now.getutc,
      load_average: `uptime`.split()[-3]
    }
    respond_to do |format|
      format.json { render json: uptime }
    end
  end
end
