var Chart = {
  // System info will consist of load average numbers collected in 10 second
  // increments.  Display up to 10 minutes' worth of datapoints.

  // loadAverages is an array of datapoint objects in format:
  // {
  //   time: "2016-05-09T20:26:22.782Z",
  //   load_average: 2.25
  // }

  loadAverages:     [],
  latestDatapoint:  {},
  alertThreshold:    1,  // TODO: This could be configurable for systems w/multiple cores

  init: function() {
    this.margin = { top: 20, right: 20, bottom: 30, left: 40 },
    this.width = parseInt(d3.select('#chart').style('width')) - 
                  this.margin.left - this.margin.right,
    this.mapRatio = .66,
    this.height = this.width * this.mapRatio - this.margin.top - this.margin.bottom;

    this.x = d3.time.scale()
      .range([0, this.width])

    this.y = d3.scale.linear()
      .range([this.height, 0]);

    this.xAxis = d3.svg.axis()
      .scale(this.x)
      .orient("bottom")
      .ticks(d3.time.minute, 1)
      .tickFormat(d3.time.format("%I:%M"));

    this.yAxis = d3.svg.axis()
      .scale(this.y)
      .orient("left")
      .ticks(10);

    this.barWidth = Math.floor((this.width - this.margin.left) / 60);

    this.tip = d3.tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html(function(d) {
        return "<strong>" + d.load_average + "</strong>"
      })

    this.color_scale = d3.scale.linear().domain([0, 3]).range(['whitesmoke', 'midnightblue']);

    this.svg = d3.select("#chart").append("svg")
        .attr("width", this.width + this.margin.left + this.margin.right)
        .attr("height", this.height + this.margin.top + this.margin.bottom)
      .append("g")
        .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");

    this.svg.call(this.tip);

    this.svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + this.height + ")")
      .call(this.xAxis);

    this.svg.append("g")
      .attr("class", "y axis")
      .call(this.yAxis)
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("Load Average");
  },

  update: function() {
    var data                = Chart.loadAverages,
        latest_time         = d3.max(data, function(d) { return new Date(d.time) }),
        ten_minutes_earlier = d3.time.minute.offset(latest_time, -10);

    Chart.x.domain([ ten_minutes_earlier, latest_time ])
    Chart.y.domain([0, d3.max(data, function(d) { return d.load_average; })]);

    Chart.svg.select(".x")
      .call(Chart.xAxis);

    Chart.svg.select(".y")
      .call(Chart.yAxis);

    var bar = Chart.svg.selectAll(".bar")
      .data(data);

    bar.enter().append("rect")
      .attr("class", "bar");

    bar.attr("x", function(d) { return Chart.x(new Date(d.time)) } )
      .attr("width", Chart.barWidth )
      .attr("y", function(d) { return Chart.y(d.load_average) } )
      .attr("height", function(d) {
        return Chart.height - Chart.y(d.load_average) } )
      .on("mouseover", Chart.tip.show)
      .on("mouseout", Chart.tip.hide)
      .attr('fill', function(d) {
        return Chart.color_scale(d.load_average);
      });

    bar.exit().remove();
  },

  resize: function() {
    Chart.width = parseInt(d3.select("#chart").style("width")) - Chart.margin.left - Chart.margin.right;;
    Chart.height = Chart.width * Chart.mapRatio - Chart.margin.top - Chart.margin.bottom;

    Chart.x.range([0, Chart.width]);
    Chart.y.range([Chart.height, 0]);

    Chart.svg.select('.x.axis')
      .attr("transform", "translate(0," + Chart.height + ")")
      .call(Chart.xAxis);

    Chart.svg.select('.y.axis')
      .call(Chart.yAxis);

    Chart.barWidth = Math.floor((Chart.width - Chart.margin.left) / 60);

    Chart.svg.select('.bar')
      .attr("x", function(d) { return Chart.x(new Date(d.time)) } )
      .attr("width", function(d) { return Math.floor((Chart.width - Chart.margin.left) / 60) } )
      .attr("y", function(d) { return Chart.y(d.load_average) } )
      .attr("height", function(d) {
        return Chart.height - Chart.y(d.load_average) } )
  },

  updateLoadAverages: function(datapoint){
    datapoint["load_average"] = parseFloat(datapoint["load_average"])
    Chart.latestDatapoint = datapoint;

    // As data collection exceeds 10 minutes (60 recordings), drop oldest
    // datapoint & add newest.
    if (Chart.loadAverages.length >= 60) {
      Chart.loadAverages.shift();
    }

    Chart.loadAverages.push(datapoint);

    var lastTwoMinutes = Chart.loadAverages.slice(-12)
    Chart.updateAverage(lastTwoMinutes);

    Chart.update();
  },

  updateAverage: function(data) {
    // Keep running tab of average performance over last two minutes.
    //
    // Two minutes include 12 ten-second increments.
    //
    // Total number of datapoints may be fewer than 12 if a full two minutes
    // of data haven't yet been collected.

    var total = 0;

    for (var i=0; i < data.length; i++) {
      total += data[i]["load_average"]
    }

    var average = total / data.length

    if (average > Chart.alertThreshold) {
      roundedAverage = Number((average).toFixed(2));
      Chart.createAlert(roundedAverage);
    }

    return average;
  },

  createAlert: function(average) {
    var time = moment(Chart.latestDatapoint["time"]),
        formattedTime = time.format("hh:mm:ss a")
        newAlert =
          "<div class='load-alert'>" +
            "<h4 class='load-alert-header'>High load generated an alert.</h4>" +
            "<div class='load-alert-body'>" +
              "<span class='load'><small>Load:</small><p>" + average + "</p></span>" +
              "<span class='time'><small>Triggered at:</small><p>" + formattedTime + "</p></span>" +
            "</div>" +
          "</div>"

    $("#alerts").prepend(newAlert)
  },

  getLoadAverage: function() {
    $.get("load_average").done(function( data ) {
      Chart.updateLoadAverages(data)
    })
    .fail(function( jqXHR, textStatus, errorThrown ) {
      errorHtml = "<div class='alert alert-danger alert-dismissible' role='alert'>" +
        "Sorry, an error occurred." +
        "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" +
        "</div>"

      $(".errors").html( errorHtml );
      clearInterval(Chart.timer);
    });
  }

}


$(document).ready(function(){
  Chart.init();
  Chart.getLoadAverage();

  Chart.timer = setInterval(Chart.getLoadAverage, 10000);

  d3.select(window).on('resize', Chart.resize);
})
